import csv

import pandas as pd
from sklearn.linear_model import LinearRegression


def to_list(row):
    arr = []
    arr.append(row['MSSubClass'])
    arr.append(row['LotFrontage'])
    arr.append(row['LotArea'])
    arr.append(row['OverallQual'])
    # arr.append(row['OverallCond'])
    arr.append(row['YearBuilt'])
    # arr.append(row['YearRemodAdd'])
    arr.append(row['MasVnrArea'])
    arr.append(row['GarageArea'])

    return arr


train_df = pd.read_csv('data/train.csv')
test_df = pd.read_csv('data/test.csv')
sum = [0] * 10
cnt = [0] * 10

for index, r in train_df.iterrows():
    if index == 0:
        continue
    row = to_list(r)
    for col in range(len(row)):
        if str(row[col]) != 'nan':
            sum[col] += row[col]
            cnt[col] += 1
x_train = []
y_train = []
x_test = []

for index, r in train_df.iterrows():
    if index == 0:
        continue
    row = to_list(r)

    for col in range(len(row)):
        if str(row[col]) == 'nan':
            row[col] = sum[col] / cnt[col]

    x_train.append(row)
    y_train.append(r['SalePrice'])

lin_reg = LinearRegression()
lin_reg.fit(x_train, y_train)

with open('data/result.csv', 'w', newline='') as result:
    writer = csv.writer(result)
    field = ["Id", "SalePrice"]
    writer.writerow(field)

    for index, r in test_df.iterrows():

        row = to_list(r)

        for col in range(len(row)):
            if str(row[col]) == 'nan':
                row[col] = sum[col] / cnt[col]

        x_test.append(row)

    y_test = lin_reg.predict(x_test)
    id = 1461
    for pr in y_test:
        writer.writerow([id, pr])
        id += 1
